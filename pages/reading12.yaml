title:      "Reading 12: Self-Driving Cars"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  ## Readings

  The readings for this week focus on [self-driving cars] and [cryptocurrencies]:

  ### Self-Driving Cars

  These articles discuss the push for [self-driving cars]:

  - [All Tesla Cars Being Produced Now Have Full Self-Driving Hardware](https://www.tesla.com/blog/all-tesla-cars-being-produced-now-have-full-self-driving-hardware)

  - [Ford’s billion-dollar self-driving car AI deal](https://arstechnica.com/cars/2017/02/fords-billion-dollar-self-driving-car-ai-deal/)

  - [GM Drops the Steering Wheel and Gives Robot Driver Control](https://www.bloomberg.com/news/articles/2018-01-12/gm-drops-the-steering-wheel-and-gives-the-robot-driver-control)

  - [Toyota’s Research Institute head says full autonomous driving is “not even close”](https://techcrunch.com/2017/01/04/toyotas-research-institute-head-says-full-autonomous-driving-is-not-even-close/)

  These articles focus on [Uber] and [Lyft]'s push into [self-driving cars]:

  - [Uber's First Self-Driving Fleet Arrives in Pittsburgh This Month](http://www.bloomberg.com/news/features/2016-08-18/uber-s-first-self-driving-fleet-arrives-in-pittsburgh-this-month-is06r7on)

  - [The Arrival of Self-Driving Ubers in Pittsburgh](http://www.theatlantic.com/news/archive/2016/09/uber-self-driving-/499922/)

  - [Lyft says robots will drive most of its cars in five years](http://www.recode.net/2016/9/18/12955162/lyft-gm-self-driving-cars)

  - [Uber Suspends Tests of Self-Driving Vehicles After Arizona Crash](https://www.nytimes.com/2017/03/25/business/uber-suspends-tests-of-self-driving-vehicles-after-arizona-crash.html?mtrref=undefined)

  These articles consider two recent accidents involving [self-driving cars]:

  - [Self-Driving Uber Car Kills Pedestrian in Arizona, Where Robots Roam](https://mobile.nytimes.com/2018/03/19/technology/uber-driverless-fatality.html)

      - [Police Say Uber Is Likely Not at Fault for Its Self-Driving Car Fatality in Arizona](http://fortune.com/2018/03/19/uber-self-driving-car-crash/)

      - [Uber Victim Stepped Suddenly in Front of Self-Driving Car](https://www.bloomberg.com/news/articles/2018-03-20/video-shows-woman-stepped-suddenly-in-front-of-self-driving-uber)

      - [Uber Video Shows the Kind of Crash Self-Driving Cars Are Made to Avoid](https://www.wired.com/story/uber-self-driving-crash-video-arizona/)


  - [Tesla Model X fatal crash investigation](https://techcrunch.com/story/tesla-model-x-fatal-crash-investigation/)

      - [Tesla says Autopilot was active during fatal crash in Mountain View](https://arstechnica.com/cars/2018/03/tesla-says-autopilot-was-active-during-fatal-crash-in-mountain-view/)

      - [Tesla stock falls 12% on crash investigation, debt downgrade](https://arstechnica.com/cars/2018/03/tesla-stock-falls-12-after-ntsb-announces-fatal-crash-investigation/)

      - [EXCLUSIVE: Tesla issues strongest statement yet blaming driver for deadly crash](http://abc7news.com/automotive/exclusive-tesla-issues-strongest-statement-yet-blaming-driver-for-deadly-crash/3325908/)

  These articles consider the safety of [self-driving cars] in general:

  - [Self-Driving Cars Still Don't Know How to See](https://www.theatlantic.com/technology/archive/2018/03/uber-self-driving-fatality-arizona/556001/)

  - [Self-driving car accidents: Robot drivers are ‘odd, and that’s why they get hit’](https://www.seattletimes.com/business/self-driving-car-accidents-robot-drivers-are-odd-and-thats-why-they-get-hit/)

  - [Car makers can't "drive their way to safety" with self-driving cars](http://arstechnica.com/cars/2016/04/car-makers-cant-drive-their-way-to-safety-with-self-driving-cars/)

  - [Self-driving cars still can’t mimic the most natural human behavior](https://qz.com/1064004/self-driving-cars-still-cant-mimic-the-most-natural-human-behavior/)

  These articles discuss the growing regulation of [self-driving cars]:

  - [Cities Need to Take the Wheel in Our Driverless Future](https://www.wired.com/story/cities-need-to-take-the-wheel-in-our-driverless-future/)

  - [U.S. House unanimously approves sweeping self-driving car measure](https://www.reuters.com/article/us-autos-selfdriving/u-s-house-unanimously-approves-sweeping-self-driving-car-measure-idUSKCN1BH2B2)

  - [Self-Driving Cars Gain Powerful Ally: The Government](http://www.nytimes.com/2016/09/20/technology/self-driving-cars-guidelines.html?_r=0)

  - [Self-Driving Cars Must Meet 15 Benchmarks in U.S. Guidance](http://www.bloomberg.com/news/articles/2016-09-20/self-driving-cars-must-meet-15-benchmarks-in-new-u-s-guidance)

  These articles consider the **social dilemma** of [self-driving cars]:

  - [The social dilemma of autonomous vehicles](http://science.sciencemag.org/content/352/6293/1573.full)

  - [Tesla's 'Autopilot' Will Make Mistakes. Humans Will Overreact.](https://www.bloomberg.com/view/articles/2016-07-01/tesla-s-autopilot-will-make-mistakes-humans-will-overreact)

  - [Your Self-Driving Car Will Be Programmed to Kill You-Deal With It](http://gizmodo.com/your-self-driving-car-will-be-programmed-to-kill-you-de-1782499265)

  - [Enough With the Trolley Problem](https://www.theatlantic.com/technology/archive/2018/03/got-99-problems-but-a-trolley-aint-one/556805/)

  This contains a series of short stories or articles related to [self-driving cars]:

  - [Full Tilt: When 100% of Cars Are Autonomous](https://www.nytimes.com/interactive/2017/11/08/magazine/tech-design-autonomous-future-cars-100-percent-augmented-reality-policing.html?mtrref=undefined&mtrref=www.nytimes.com)

  [Uber]: https://www.uber.com
  [Lyft]: https://www.lyft.com

  ### Cryptocurrencies

  These articles introduce the idea of [cryptocurrencies]:

  - [The WIRED Guide to Bitcoin](https://www.wired.com/story/guide-bitcoin/)

  - [How does Bitcoin work?](https://bitcoin.org/en/how-it-works)

  - [Still Don't Get Bitcoin? Here's an Explanation Even a Five-Year-Old Will Understand](https://www.coindesk.com/bitcoin-explained-five-year-old/)

  - [Bitcoin explained: The digital currency making millionaires](http://www.abc.net.au/news/2013-12-02/bitcoins-the-digital-currency-explained/5119034)

  These articles talk about the [Bitcoin] bubble:

  - [Bitcoin 'Is Just Like the Dot-Com Bubble'](https://www.theatlantic.com/business/archive/2017/12/bitcoin-dotcom-bubble/549155/)

  - [Why Bitcoin’s Bubble Matters](http://archive.is/jd0zb#selection-2155.0-2155.28)

  - [Bitcoin Falls Below $10,000 as Virtual Currency Bubble Deflates](https://www.nytimes.com/2018/01/17/technology/bitcoin-virtual-currency-bubble.html)

  - [Bitcoin Can Drop 50% and China Miners Will Still Make Money](https://www.bloomberg.com/news/articles/2018-01-10/bitcoin-can-drop-50-and-china-s-miners-will-still-make-money)

  - [Bitcoin’s insane energy consumption, explained](https://arstechnica.com/tech-policy/2017/12/bitcoins-insane-energy-consumption-explained/)

  These articles talk about [cryptocurrencies] and their recent hype:

  - [Cryptocurrencies Come to Campus](https://www.nytimes.com/2018/02/08/technology/cryptocurrencies-come-to-campus.html)

  - [It’s time to bring Bitcoin and cryptocurrencies into the computer science curriculum](https://freedom-to-tinker.com/2014/09/15/its-time-to-bring-bitcoin-and-cryptocurrencies-into-the-computer-science-curriculum/)

  - [How I fell for the blockchain gold rush](https://www.theguardian.com/technology/2018/jan/07/bitcoin-crypto-currencies-mcafee)

  - [Bitcoin has little shot at ever being a major global currency](https://www.cnbc.com/2018/01/10/bitcoin-has-little-shot-at-ever-being-a-major-global-currency.html)

  These articles talk about the [Silk Road]:

  - [How FBI brought down cyber-underworld site Silk Road](https://www.usatoday.com/story/news/nation/2013/10/21/fbi-cracks-silk-road/2984921/)

  - [Sunk: How Ross Ulbricht ended up in prison for life](https://arstechnica.com/tech-policy/2015/05/sunk-how-ross-ulbricht-ended-up-in-prison-for-life/)

  - [The History of Silk Road: A Tale of Drugs, Extortion & Bitcoin](https://blockonomi.com/history-of-silk-road/)

  - [The Untold Story of Silk Road, Part 1](https://www.wired.com/2015/04/silk-road-1/)

  - [The Untold Story of Silk Road, Part 2](https://www.wired.com/2015/05/silk-road-2/)

  The articles discuss [Mt. Gox]:

  - [The History of the Mt Gox Hack: Bitcoin’s Biggest Heist](https://blockonomi.com/mt-gox-hack/)

  - [The Inside Story of Mt. Gox, Bitcoin's $460 Million Disaster](https://www.wired.com/2014/03/bitcoin-exchange/)

  - [Twice burned - How Mt. Gox’s bitcoin customers could lose again](https://www.reuters.com/investigates/special-report/bitcoin-gox/)

  - [Mt. Gox ex-CEO, who may profit from site’s fall, says he “doesn’t want this”](https://arstechnica.com/tech-policy/2018/04/mt-gox-ex-ceo-who-may-profit-from-sites-fall-says-he-doesnt-want-this/)

  [blockchain]:     https://en.wikipedia.org/wiki/Blockchain
  [Silk Road]:      https://en.wikipedia.org/wiki/Silk_Road_(marketplace)
  [Mt. Gox]:        https://en.wikipedia.org/wiki/Mt._Gox
  [Bitcoin]:        https://en.wikipedia.org/wiki/Bitcoin

  ## Questions

  Please write a response to one of the following questions:

  1. After reading some of the articles above, address the following questions:

      - What is the motivation for developing and building self-driving cars?
        What are the arguments for and against self-driving cars?  Would they
        make our roads safer?

      - How should programmers address the "social dilemma of autonomous
        vehicles"?  How should an artificial intelligence approach
        life-and-death situations?  Who is liable for when an accident happens?

      - What do you believe will be the social, economic, and political impact
        of self-driving cars?  What role should the government play in
        regulating self-driving cars?

      - Would you want a self-driving car?  Explain why or why not.

  2. After reading some of the aricles above, address the following questions:

      - What is the motivation for developing and building [blockchain]
        technology and [cryptocurrencies]?  What are the arguments for and
        against the use of [cryptocurrencies]?

      - How should programmers address the moral and ethical concerns about the
        use of [blockchain] technology and [cryptocurrencies] for illegal
        activities such as selling drugs and sex trafficking?  Is it really
        desirable to have untraceable or anonymous decentralized financial
        transactions?

      - What do you believe will be the social, economic, and political impact
        of [cryptocurrencies]?  What role should the government play in
        regulating [blockchain] technology and [cryptocurrencies]?

      - Do you believe in the hype?  Or is this just another technology bubble
        waiting to burst?  Do you trust [cryptocurrencies] and the exchanges
        and ecosystem around the technology?  Would you invest in [Bitcoin]?

  [self-driving cars]:  https://en.wikipedia.org/wiki/Vehicular_automation
  [cryptocurrencies]:   https://en.wikipedia.org/wiki/Cryptocurrency

  ## Feedback

  If you have any questions, comments, or concerns regarding the course, please
  provide your feedback at the end of your response.
