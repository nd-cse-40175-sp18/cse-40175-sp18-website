CSE 40175 Spring 2018
=====================

This is the source code for the CSE 40175 Ethical and Professional Issues
(Spring 2018) [course
website](http://www3.nd.edu/~pbui/teaching/cse.40175.sp18/).
